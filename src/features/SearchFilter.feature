Feature: Search Filter
    As a user on the business lease cars page
    I want to filter cars
    Because I want to find the best one for leasing

    @happypath @regression
    Scenario: Filtering by the best deals
        Given I am on the car showroom page
        When I click on the popular filters button
        And I check the best deals checkbox
        Then relevant results are displayed on the page

    @sadpath @regression
    Scenario: Filtering by the make&model
        Given I am on the car showroom page
        When I click on the make&model button
        And I enter "bmw3" into the search bar
        Then there is no relevant results on the page
