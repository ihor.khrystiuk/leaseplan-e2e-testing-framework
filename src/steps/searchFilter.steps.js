/* eslint-disable no-undef */
import { Given, When, Then } from 'cucumber';
import { homePage } from '../pages/Home.page';
import { businessLeasePage } from '../pages/BusinessLease.page';

let expectedResults, actualResults;

Given('I am on the home page', function () {
    homePage.load();
    expect(browser).toHaveTitleContaining('LeasePlan');
});

Given('I choose a be-en locale', function () {
    const isLocale = homePage.$locale.isDisplayed();
    if (isLocale) {
        homePage.selectLocation();
        expect(browser).toHaveUrlContaining('en-be');
    }
});

Given('I navigate to the business lease page', function () {
    homePage.navigateToBusinessLeasePage();
    expect(browser).toHaveUrlContaining('business');
});

Given('I am on the car showroom page', function () {
    businessLeasePage.load();
    expect(browser).toHaveUrlContaining('showroom');
});

When('I click on the popular filters button', function () {
    businessLeasePage.openPopularFilterOptions();
    expectedResults = businessLeasePage.getExpectedItemsCount();
});

When('I check the best deals checkbox', function () {
    businessLeasePage.filterByBestDeals();
    const filter = businessLeasePage.$filterByPopular;
    browser.waitUntil(() => filter.getText() === 'Best deals', {
        timeout: 5000,
        timeoutMsg: 'expected text to be different after 5s'
    });
    expect(filter).toHaveText('Best deals');
});

Then('relevant results are displayed on the page', function () {
    actualResults = businessLeasePage.getActualItemsCount();
    browser.waitUntil(() => actualResults === expectedResults, {
        timeout: 5000,
        timeoutMsg: 'expected text to be different after 5s'
    });
    expect(expectedResults).toBe(actualResults);
});

When('I click on the make&model button', function () {
    businessLeasePage.openMakeAndModelFilterOptions();
});

When(/^I enter "([^"]*)" into the search bar$/, function (name) {
    businessLeasePage.setMakeName(name);
});

Then('there is no relevant results on the page', function () {
    actualResults = businessLeasePage.getActualItemsCount();
    expect(actualResults).toBe('0');
});
