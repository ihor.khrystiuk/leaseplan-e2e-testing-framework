import BasePage from './Base.page';

class BusinessLeasePage extends BasePage {
    constructor(url = './en-be/business/showroom/') {
        super(url);
    }
    /**
     * define elements
     */
    get $spinner() {
        return $('[data-e2e-id="Campaign Banner"]');
    }

    get $showRooomLink() {
        return $('[data-e2e-id="subMenuItem-/en-be/business/showroom/"]');
    }

    get $filterMenu() {
        return $('[data-component="desktop-filters"]');
    }

    get $filterByPopular() {
        return $('[data-component="desktop-filters"]').$('[data-key="popularFilters"]');
    }

    get $filterByMakeAndModel() {
        return $('[data-component="desktop-filters"]').$('[data-key="makemodel"]');
    }

    get $bestDealsInput() {
        return $('//*[@id = "Best deals"]/..');
    }

    get $applyFilterBtn() {
        return $('[data-component="OnPageFilterBody"]').$('button=Save');
    }

    get $bestDealsExpectedValue() {
        return $("//span[text() = 'Best deals']/following-sibling::span");
    }

    get $bestDealsActualValue() {
        return $('[data-key="features.showroom.toChooseFrom"]');
    }

    get $searchBarForMake() {
        return $('[data-e2e-text-input-input]');
    }

    get $errorMsg() {
        return $('[title="Make & Model"]');
    }

    /**
     * define or overwrite page methods
     */
    load() {
        super.load();
    }

    navigateToShowRoom() {
        this.$spinner.waitForDisplayed({ reverse: true });
        this.$showRooomLink.waitAndClick();
    }

    openPopularFilterOptions() {
        this.$filterMenu.waitForDisplayed();
        this.$filterByPopular.waitAndClick();
        this.$applyFilterBtn.waitForDisplayed({ timeout: 5000 });
    }

    openMakeAndModelFilterOptions() {
        this.$filterMenu.waitForDisplayed();
        this.$filterByMakeAndModel.waitAndClick();
    }

    getExpectedItemsCount() {
        return this.$bestDealsExpectedValue.getText().replace(/[^0-9]/g, '');
    }

    filterByBestDeals() {
        this.$bestDealsInput.waitAndClick();
        this.$applyFilterBtn.waitAndClick();
    }

    getActualItemsCount() {
        return this.$bestDealsActualValue.getText().replace(/[^0-9]/g, '');
    }

    setMakeName(name) {
        this.$filterByMakeAndModel.doubleClick();
        this.$searchBarForMake.focus();
        this.$searchBarForMake.setValue(name);
    }
}

export const businessLeasePage = new BusinessLeasePage();
