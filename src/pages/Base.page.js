class BasePage {
    constructor(path) {
        this.path = path;
    }

    get $acceptCookiesBtn() {
        return $("//button[contains(., 'Accept Cookies')]");
    }

    load() {
        browser.setWindowSize(1920, 1080);
        browser.url(this.path);
        const isExist = this.$acceptCookiesBtn.isDisplayed({ timeout: 5000 });
        if (isExist) {
            browser.waitUntil(
                () => {
                    this.$acceptCookiesBtn.waitAndClick();
                    return this.$acceptCookiesBtn.waitForDisplayed({ reverse: true });
                },
                { timeoutMsg: "There is no the 'Accept Cookies' button on the home page " }
            );
        }
    }
}

export default BasePage;
