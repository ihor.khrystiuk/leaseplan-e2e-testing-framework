import BasePage from './Base.page';
import { navigateTo } from './components/Navigation';

class HomePage extends BasePage {
    constructor(url = './') {
        super(url);
    }
    /**
     * define elements
     */
    get $locale() {
        return $("//button[contains(., 'Belgium')]");
    }

    get $localeLangList() {
        return $('#label-locale-selector-be');
    }

    get $chooseLang() {
        return $('#label-locale-selector-be').$('=English');
    }
    /**
     * define or overwrite page methods
     */
    load() {
        super.load('./');
    }

    selectLocation() {
        this.$locale.waitAndClick();
        this.$localeLangList.waitForDisplayed({ timeout: 2000 });
        this.$chooseLang.waitAndClick();
    }

    navigateToBusinessLeasePage() {
        navigateTo.$businessLeasePage.waitAndClick();
    }
}

export const homePage = new HomePage();
