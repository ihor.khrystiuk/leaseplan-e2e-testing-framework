class Navigation {
    /**
     * define elements
     */
    get $businessLeasePage() {
        return $('[data-e2e-id*="business"]');
    }

    get $privateLeasePage() {
        return $('[data-e2e-id*="privatelease"]');
    }

    get $drivingElectricPage() {
        return $('[data-e2e-id*="electric-driving"]');
    }

    get $fleetManagementPage() {
        return $('[data-e2e-id*="fleet-management"]');
    }

    get $mobilityPage() {
        return $('[data-e2e-id*="mobility"]');
    }

    get $myLeaseCarPage() {
        return $('[data-e2e-id*="my-lease-car"]');
    }

    get $aboutUsPage() {
        return $('[data-e2e-id*="about"]');
    }
}

export const navigateTo = new Navigation();
