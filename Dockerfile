FROM atools/chrome-headless:java8-node10-latest

RUN mkdir /e2e
WORKDIR /e2e

COPY . /e2e

# avoid many lines of progress bars during install
# https://github.com/cypress-io/cypress/issues/1243
ENV CI=1

# install NPM dependencies
RUN npm ci

RUN npm test || true

RUN npm run report:generate