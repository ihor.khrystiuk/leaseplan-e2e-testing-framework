# E2E tests with WebDriverIO and Cucumber

This is a demonstration project of functional UI tests.
These tests are developed in JavaScript with [WebDriverIO V6](http://webdriver.io/) and [Cucumber](https://cucumber.io/)

## Features

-   JavaScript
-   [Expect-webdriverio](https://github.com/webdriverio/expect-webdriverio)
-   Page Object Pattern
-   [Gherkin lint](https://github.com/vsiakka/gherkin-lint)
-   ESlint
-   Prettier
-   Husky
-   GitLab CI
-   Allure report (screenshots on failure)

## Requirements

-   node >= 15.6.x - [how to install Node](https://nodejs.org/en/download/)
-   npm >= 7.4.x - [how to install NPM](https://www.npmjs.com/get-npm)
-   Install JDK 1.8 for running allure report.
-   Optional. Docker for running tests inside docker container [docker] (https://www.docker.com/)

## Getting Started
Clone the repository and open the folder with project.
```bash
https://gitlab.com/ihor.khrystiuk/leaseplan-e2e-testing-framework.git
```
Install the dependencies:

```bash
npm install
```

Run e2e tests:

```bash
npm test
```
### Using tags:
If you want to run only specific test(s) you can mark your scenarios with tags. These tags will be placed before each scenario like so:

```bash
@Tag
Scenario: ...
```
To run only the tests with specific tag(s) use the next script:

```bash
npm test -- --cucumberOpts.tagExpression='@sadpath'
```
To run tests using Docker:

```bash
docker-compose up -d
```

## Reports

### Allure

Run this command to generate the allure report in the directory `./test-report/allure-report`:

```bash
npm run report:generate
```

You can run this command to start a server on your machine and open the allure report on the browser:

```bash
npm run report:open
```

## Prettier and Eslint

Run to format the code:

```bash
npm run format
npm run lint:fix
```

## Gherkin lint

We use [Gherkin lint](https://github.com/vsiakka/gherkin-lint) to keep the feature files organized.

```bash
npm run format:gherkin
```
